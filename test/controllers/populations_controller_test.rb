require 'test_helper'

class PopulationsControllerTest < ActionController::TestCase
  setup do
    @population = populations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:populations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create population" do
    assert_difference('Population.count') do
      post :create, population: { created_at: @population.created_at, updated_at: @population.updated_at, y1993: @population.y1993, y2005: @population.y2005, y2010: @population.y2010, y2015: @population.y2015, zone: @population.zone }
    end

    assert_redirected_to population_path(assigns(:population))
  end

  test "should show population" do
    get :show, id: @population
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @population
    assert_response :success
  end

  test "should update population" do
    patch :update, id: @population, population: { created_at: @population.created_at, updated_at: @population.updated_at, y1993: @population.y1993, y2005: @population.y2005, y2010: @population.y2010, y2015: @population.y2015, zone: @population.zone }
    assert_redirected_to population_path(assigns(:population))
  end

  test "should destroy population" do
    assert_difference('Population.count', -1) do
      delete :destroy, id: @population
    end

    assert_redirected_to populations_path
  end
end
