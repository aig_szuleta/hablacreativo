module PopulationsHelper
  #consuming api service
  def get_data
    JSON.load(open("http://aspirantes.hablacreativo.com/api/v1/admission_test"))
  end

  def get_populations_api values
    populations = []
    values.each { |value| populations << Population.new(value) }
    populations
  end

  def get_populations values
    populations = Population.all
    populations += get_populations_api values
  end


end
