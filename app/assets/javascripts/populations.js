function show_form(){
    if($('div#attributes').is(":visible")){
        $('div#attributes').hide();
        $('div#show-button').show();
    }else{
        $('div#attributes').show();
        $('div#show-button').hide();
    }

    return false;
}


function validate_form(url){
        $.ajax({
            url: url,
            type: 'post',
            data: $('form#new_population').serialize()
    });
}
