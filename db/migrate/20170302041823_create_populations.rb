class CreatePopulations < ActiveRecord::Migration
  def change
    create_table :populations do |t|
      t.string :zone
      t.integer :y1993
      t.integer :y2005
      t.integer :y2010
      t.integer :y2015
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps null: false
    end
  end
end
